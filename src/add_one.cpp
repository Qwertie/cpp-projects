#include <string>
#include "add_one.h"

using namespace std;

string add_one (string input)
{

	bool carry = true;
	if (input[0] == '-')
	{
		for(int i = input.size(); i > 0; i--)
		{
			int current = input[i - 1] - '0';

			if (carry)
			{
				current -= 1;
				if (current == -1) {
					carry = true;
					current = 9;
				}
				else
				{
					carry = false;
				}

				input[i - 1] = current + '0';

				if (carry && i == 1)
				{
					input.insert(0, "1");
				}
			}
		}
	}
	else
	{
		for (int i = input.size(); i > 0; i--)
		{
			int current = input[i - 1] - '0';

			if (carry)
			{
				current += 1;
				if (current == 10)
				{
					carry = true;
					current = 0;
				}
				else
				{
					carry = false;
				}

				input[i - 1] = current + '0';

				if (carry && i == 1)
				{
					input.insert(0, "1");
				}
			}
		}
	}


	return input;
}
